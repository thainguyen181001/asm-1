﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeacherManagerment
{
    public class Teacher : Person
    {
        private const decimal XuLyHeSoLuong = 0.6M;
        private const decimal heSoLuong = 1.25M;
        public int Id { get; set; }
        public decimal HeSoLuong { get; set; }
        public decimal Luong
        {
            get => TinhLuong();
        }
        public Teacher(string HoVaTen, int NamSinh, decimal LuongCoBan, decimal HeSoLuong) : base(HoVaTen, NamSinh, NamSinh)
        {
            this.HeSoLuong = HeSoLuong;
        }

        public Teacher() : base() { }

        public Teacher NhapThongTin()
        {
            Teacher teacher = new Teacher();
            Console.WriteLine("Nhập id: ");
            teacher.Id = int.Parse(Console.ReadLine());
            base.NhapThongTin();
            Console.WriteLine("Nhập hệ số lương ");
            teacher.HeSoLuong = decimal.Parse(Console.ReadLine());
            return teacher;
        }
        public decimal TinhLuong()
        // trả về theo công thức lương cơ bản * hệ số lương *1.25
        {
            return (decimal)base.LuongCoBan * (decimal)HeSoLuong * heSoLuong;
        }
        public override string ToString()
        {
            return base.ToString() + $" - {Luong}";
        }
        public decimal Xuly()
        {
            return (decimal)HeSoLuong * XuLyHeSoLuong;
        }
    }
}
