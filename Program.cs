﻿using TeacherManagerment;
Console.OutputEncoding = System.Text.Encoding.UTF8;
TeacherList teacher = new TeacherList();
int choice = 0;
do
{
    try
    {
        Console.WriteLine("1. Nhập giáo viên mới");
        Console.WriteLine("2. Xem giáo viên có lương thấp nhất ");
        Console.WriteLine("3. Exit !");
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                teacher.AddTeacher();
                break;
            case 2:
                Console.WriteLine(teacher.TeacherWithLowestSalary());
                break;
            case 3:
                Environment.Exit(0);
                break;
            default:
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 3);