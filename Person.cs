﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeacherManagerment
{
    public class Person
    {
        //khai báo thuộc tính
        private int namSinh;
        public string HoVaTen { get; set; }
        public decimal LuongCoBan { get; set; }

        public int NamSinh 
        { 
            get => namSinh;
            set
            {
                namSinh = value;
                if (value < 0) 
                {
                    throw new Exception("Năm sinh phải lớn hơn 0");
                }
                if (value.ToString().Length < 4) 
                {
                    throw new Exception("Năm sinh phải có 4 chữ số");
                }
            }
        }
        public void NhapThongTin()
        {
            Console.WriteLine("Nhập tên: ");
            HoVaTen = Console.ReadLine();
            Console.WriteLine("Nhập năm sinh: ");
            NamSinh = int.Parse(Console.ReadLine());
            Console.WriteLine("Nhập lương: ");
            LuongCoBan = decimal.Parse(Console.ReadLine());
        }
        public Person()
        {
            
        }
        public Person(string hoVaTen, int namSinh, decimal luongCoBan)
        {
            HoVaTen = hoVaTen;
            NamSinh = namSinh;
            LuongCoBan = luongCoBan;
        }
        public override string ToString()
        {
            string information = $"\n{HoVaTen} - {NamSinh} - {LuongCoBan}";
            return information;
        }
    }
}
