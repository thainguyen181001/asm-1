﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeacherManagerment
{
    public class TeacherList
    {
        List<Teacher> teacherArray;
        public TeacherList()
        {
            teacherArray = new List<Teacher>();
            teacherArray.Add(new Teacher { Id = 1, HoVaTen = "Haha", HeSoLuong = 2, LuongCoBan = 3000, NamSinh = 2001});
            teacherArray.Add(new Teacher { Id = 2, HoVaTen = "asd", HeSoLuong = 3, LuongCoBan = 2000, NamSinh = 2001});
            teacherArray.Add(new Teacher { Id = 3, HoVaTen = "dsa", HeSoLuong = 1, LuongCoBan = 1000, NamSinh = 2001});
            teacherArray.Add(new Teacher { Id = 4, HoVaTen = "aaa", HeSoLuong = 1, LuongCoBan = 3000, NamSinh = 2001});
            teacherArray.Add(new Teacher { Id = 5, HoVaTen = "sss", HeSoLuong = 2, LuongCoBan = 4000, NamSinh = 2001});
        }

        public void AddTeacher()
        {
            Teacher teacher = new Teacher();
            teacher.NhapThongTin();
            teacherArray.Add(teacher);
        }

        public Teacher GetTeacherById(int id)
        {
            var teacherById = teacherArray.SingleOrDefault(teacher => teacher.Id == id);
            return teacherById;
        }

        public string TeacherWithLowestSalary()
        {
            StringBuilder sb = new StringBuilder();
            var list = teacherArray.ToList().OrderBy(teacher => teacher.Luong).Take(1);
            foreach (var item in list)
            {
                sb.Append(item.ToString());
            }
            return sb.ToString();
        }

    }
}
